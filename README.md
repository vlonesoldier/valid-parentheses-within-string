# Validation of parentheses

### Given a string s of '(' , ')' and lowercase English characters.

Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that the resulting parentheses string is valid and return any valid string.

Formally, a parentheses string is valid if and only if:

It is the empty string, contains only lowercase characters, or
It can be written as AB (A concatenated with B), where A and B are valid strings, or
It can be written as (A), where A is a valid string.
 

Examples:
```sh
Input: s = "lee(t(c)o)de)"
Output: "lee(t(c)o)de"
Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.
```
```sh
Input: s = "a)b(c)d"
Output: "ab(c)d"
```
```sh
Input: s = "))(("
Output: ""
Explanation: An empty string is also valid.
```

# Complexity
- Time complexity:

    O(n), n - number of characters in string 

- Space complexity:

    O(1)