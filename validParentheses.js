/**
 * @param {string} s
 * @return {string}
 */
var minRemoveToMakeValid = function (s) {
    const stack = [];
    const stringArr = Array.from(s);

    for (let i in stringArr) {

        switch (stringArr[i]) {

            case '(':
                stack.push(i);
                break;

            case ')':
                if (!stack.length) {
                    stringArr[i] = '';
                }
                else {
                    stack.pop();
                }
                break;
        }
    }

    for (let i of stack) {
        stringArr[i] = '';
    }
    return stringArr.join('');
};